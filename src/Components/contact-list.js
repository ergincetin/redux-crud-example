import React from "react"
import {HourglassFilled, SyncOutlined, UserOutlined  } from "@ant-design/icons";
import {Card, message} from "antd"
import { Link } from 'react-router-dom';
import ContactCard from './contact-card';

export default function ContactList({contacts, loading, errors, deleteContact}){

    const loadingMessage = () => {
            <SyncOutlined/>
            message.info('we are fetching that content for you.');

    }
       
      const emptyMessage = ( ) => {
            <SyncOutlined/>
            message.warning('No Contacts Found');

      }

      
      const timeOutMessage = () => {
            <HourglassFilled/>
            message.error(errors.global);


      }

    const cards = () => {
        return contacts.map(contact => {
         return (
        <ContactCard key={contact._id} contact={contact} deleteContact={deleteContact} />
      )
    })
  }

  const contactList = (
    <Card>
      { cards() }
    </Card>
  )

return (


    <div>
        <p>{loading && loadingMessage}</p>
        <h3>Just one second...</h3>
        <p>We are fetching that content for you.</p>
        <hr></hr>

        { contacts.length === 0 && !loading  && !errors.global && emptyMessage }
        <p>Add some new contacts to get started.</p>
           <Link to={'/contacts/new'} className="ui button primary">
               Add New Contact
            </Link>
        <hr></hr>

        { errors.global && timeOutMessage }
        <hr></hr>
        { contacts.length > 0 && contactList }

    </div>
)


  }
  