import React from "react";
import { Form, Field} from "formik";
import {Row, Col } from "antd"
import { AntInput } from "../CreateAntFields/CreateAntFields";

const CreateForm = ({ submitting, pristine }) => {
  <Form>
    <Row>
      <Col span={4}>
        <label>First Name</label>
        <Field
          type="text"
          component={AntInput}
          placeholder="first name"
          name="name.first"
        />
      </Col>

      <Col span={4}>
        <label>Last Name</label>
        <Field
          type="text"
          component={AntInput}
          placeholder="lastname"
          name="name.last"
        />
      </Col>
    </Row>

    <Row>
      <label>Phone</label>
      <Field
        type="text"
        component={AntInput}
        placeholder="Phone"
        name="phone"
      />

      <label>E-mail</label>
      <Field
        type="text"
        component={AntInput}
        placeholder="E-mail"
        name="email"
      />
    </Row>

    <Row>
      <div className="submit-container">
        <button
          className="ant-btn ant-btn-primary"
          disabled={pristine || submitting}
          type="submit"
        >
          Save
        </button>
      </div>
    </Row>
  </Form>;
};

export default CreateForm;
