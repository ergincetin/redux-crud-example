import React from 'react';
import {Card, Button, Row, Col, Descriptions} from "antd"
import {UserOutlined, PhoneOutlined, MailOutlined} from "@ant-design/icons"
import { Link } from 'react-router-dom';

export default function ContactCard({contact, deleteContact}) {
    return (
      <Card>
          <Descriptions
           bordered
           title={<p> <UserOutlined/> {contact.name.first} {contact.name.last}</p> }
           extra={
            <div>
            <Link to={`/contacts/edit/${contact._id}`} 
            style={{color: "green" }}>
                Edit</Link>
            <Button style={{color: "red"}} onClick={() => deleteContact(contact._id)} >
                Delete</Button>
          </div>
          }
          >
            <Descriptions.Item><p><PhoneOutlined/> {contact.phone}</p></Descriptions.Item>
            <Descriptions.Item><p><MailOutlined/> {contact.email}</p></Descriptions.Item>

          <div>
            <Link to={`/contacts/edit/${contact._id}`} className="ui basic button green">Edit</Link>
            <Button style={{color: "red"}} onClick={() => deleteContact(contact._id)} >Delete</Button>
          </div>
        </Descriptions>
      </Card>
    )
  }
  
  ContactCard.propTypes = {
    contact: React.PropTypes.object.isRequired
  }