import React, { PureComponent } from "react";
import { Formik } from "formik";
import CreateForm from "./create-con-form";
import { reduxForm } from "redux-form";
import { Col, Row } from "antd";

const initialValues = {
  firstname: "",
  lastname: "",
  email: "",
  phone: "",
};

class PutForm extends PureComponent {
  componentWillReceiveProps = (nextProps) => {
  
    const { contact } = nextProps;
    if (contact._id !== this.props.contact._id) {
 
      this.props.initialize(contact);
    }
  };

  render() {
      const { handleSubmit, pristine, submitting, loading, contact } = this.props;
    return (
      <Row>
          <Row>
            <Col> <h1 style={{marginTop:"1em"}}>{contact._id ?
             'Edit Contact' : 'Add New Contact'}</h1>
             </Col>
              </Row>
          
        <Col span={8}></Col>
        <Col span={8}>
          <Formik
            initialValues={initialValues}
            render={<CreateForm onSubmit={handleSubmit} 
            loading={loading} pristine={pristine} submitting={submitting} />}
          ></Formik>
          <br></br>
        </Col>
        <Col span={8} />
      </Row>
    );

    
  }
}

export default reduxForm({ form: "contactForm" })(PutForm);
