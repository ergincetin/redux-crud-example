import "./App.css";
import React, { PureComponent } from "react";
import { Layout } from "antd";
import { Link } from "react-router-dom";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import Nav from "./Nav/Nav";
import ContactFormPage from "./Components/putForm"
import ContactListPage from "./Components/contact-list"

class App extends PureComponent {
  render() {
    const { Header, Content } = Layout;
    return (
      <Provider>
        <Router>

          <Layout>

            <Header>
              <Nav/>  
            </Header>
            <Content>
            <Route exact path="/" component={ContactListPage}/>
        <Route path="/contacts/new" component={ContactFormPage}/>
        <Route path="/contacts/edit/:_id" component={ContactFormPage}/>
            </Content>
          </Layout>
        </Router>
      </Provider>
    );
  }
}

export default App;
