import React, { PureComponent } from "react";
import { Layout, Menu} from "antd";
import { Link } from "react-router-dom";

const { Header } = Layout;
const MenuItem = Menu.Item;

class Nav extends PureComponent {
  render() {
    return (
      <Layout>
        <Header>
          <Menu Menu theme="dark" mode="horizontal" defaultSelectedKeys={[""]}>
            <MenuItem>
              <Link to="/">Contact List</Link>
            </MenuItem>

            <MenuItem>
              <Link to="/contacts/new">Add New Contact</Link>
            </MenuItem>
          </Menu>
        </Header>
      </Layout>
    );
  }
}


export default Nav;